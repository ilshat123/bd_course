class TxtWorker:
    @staticmethod
    def read(file_name):
        text = open(file_name, 'r', encoding='utf-8')
        file = text.read()
        text.close()
        return file

    @staticmethod
    def write(file_name, write_file):
        text = open(file_name, 'w', encoding='utf-8')
        text.write(write_file)
        text.close()

    @staticmethod
    def get_line(file_name):
        text = open(file_name, 'r', encoding='utf-8')
        file = text.readline()
        text.close()
        return file

    @staticmethod
    def get_line_and_del(file_name):
        file = TxtWorker.read(file_name).split('\n')

        my_file = file[0]
        file = file[1:]
        file = '\n'.join(file)

        TxtWorker.write(file_name, file)

        return my_file

    @staticmethod
    def write_on_last_line(file_name, write_file):
        text = open(file_name, 'a', encoding='utf-8')
        text.write('\n' + write_file)
        text.close()

    @staticmethod
    def write_on_first_line(file_name, write_file):
        text = open(file_name, 'r+', encoding='utf-8')
        text.write(write_file + '\n')
        text.close()

# file = TxtWorker.get_line_and_del('test.txt')
# print(file)
# file = TxtWorker.get_line('test.txt')
# file2 = TxtWorker.get_line('test.txt')
# print(file + file2)
# print(f)
#
# print(file)

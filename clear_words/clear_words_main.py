from pprint import pprint

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from test import check_good_word

sql_bd_way = f'postgresql://postgres:12345123@database-1.cdm5bcwbgp5i.us-east-1.rds.amazonaws.com:5432/tweet_db'

engine = create_engine(sql_bd_way, echo=False)
metadata = MetaData(bind=engine)


Base = declarative_base()


class Word(Base):
    __tablename__ = 'clear_words'
    word = Column(String, primary_key=True)
    count = Column(Integer)
    month_day = Column(String, primary_key=True)

    @classmethod
    def create_with_tuple(cls, tuple_elem):
        obj = cls()
        obj.word = tuple_elem[0]
        obj.count = tuple_elem[1]
        obj.month_day = tuple_elem[2]
        return obj

    def __str__(self):
        return 'Word({}, {}, {})'.format(self.word, self.count, self.month_day)

    __repr__ = __str__


class Connection:
    engine = create_engine(sql_bd_way, echo=False)

    def __init__(self):
        self.create_table()
        session = sessionmaker(bind=self.engine)
        self.s = session()

    def create_table(self):
        Base.metadata.create_all(self.engine)

    def save_word(self, word_obj: Word):
        self.s.add(word_obj)

    def delete_elem(self, word_obj: Word):
        self.s.delete(word_obj)

    def commit(self):
        self.s.commit()

    def get_all(self):
        return self.s.query(Word).all()


def clear_words():
    session = sessionmaker(bind=engine)
    s = session()
    table = Table('tweets_last_version', metadata, autoload=True, autoload_with=engine)
    elems = s.query(table).all()
    # pprint(elems)
    print(len(elems))
    good_words = [Word.create_with_tuple(line) for line in elems if check_good_word(line[0])]
    # pprint(good_words)
    print(len(good_words))

    connect = Connection()
    old_words = connect.get_all()
    for elem in old_words:
        connect.delete_elem(elem)
    for word in good_words:
        connect.save_word(word)
        print(word)
    connect.commit()


if __name__ == '__main__':
    clear_words()
    connect = Connection()
    print(len(connect.get_all()))


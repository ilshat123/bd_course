from nltk.stem import WordNetLemmatizer
import nltk
from nltk.corpus import wordnet
from nltk import pos_tag

nltk.download('averaged_perceptron_tagger', quiet=True)
nltk.download('wordnet', quiet=True)


def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('JJ'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('RB'):
        return 'rb'
    else:
        return ''


def lemmatize(words):
    lemmatizer = WordNetLemmatizer()
    lemmatized = []
    pos_tags = pos_tag(words)
    print(pos_tags)
    i = 0
    for token, temp in zip(words, pos_tags):
        token_corr = temp[0].lower()
        pos = temp[1]
        print(pos)
        print(get_wordnet_pos(pos))
        if get_wordnet_pos(pos) != '':
            lemm = lemmatizer.lemmatize(token_corr, get_wordnet_pos(pos))
            lemmatized.append(lemm)
    return lemmatized


def get_word_type(word):
    res = pos_tag([word])[0]
    res2 = get_wordnet_pos(res[1])
    return res2


def check_good_word(word):
    if len(word) == 1:
        return False
    if word in ['amp', 'is', 'im']:
        return False
    return get_word_type(word) != ''

#
# #
# # print(pos_tag(['person', 'people', 'hello']))
# print(check_good_word('people'))
# print(check_good_word('hello'))
# print(check_good_word('person'))
# #
# # #
# # # wor = ['про', 'apple']
# # # print(lemmatize(wor))

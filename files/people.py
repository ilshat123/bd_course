import json

from files import settings
from txtWorker import TxtWorker


class AllPeoples:
    def __init__(self):
        pass
        # self.people_url = people_url

    def get_last_tweet(self, people_url):
        peoples_dict = self.get_all()
        return peoples_dict.get(people_url, False)

    def set_people_last_tweet(self, people_url, tweet_id):
        all_files = self.get_all()
        all_files[people_url] = tweet_id
        TxtWorker.write(settings.peoples_tweets, json.dumps(all_files))

    def get_all(self):
        return json.loads(TxtWorker.read(settings.peoples_tweets))

import re
import time
import traceback
from pprint import pprint

import requests

from files.akk_obj import AkkObj
from files.people import AllPeoples
from files.tweet import Tweet


class TwitterApi:
    def __init__(self, akk_obj, proxy):
        self.akk_obj = akk_obj
        self.s = requests.session()
        self.s.proxies = self._create_proxy(proxy)
        self.akk_obj.loads()
        self.load_session()

    def _create_proxy(self, proxy):
        proxy = proxy.split(':')
        ip_port = proxy[0] + ':' + proxy[1]
        login_password = proxy[2] + ':' + proxy[3]
        proxy_inf = 'http://{0}@{1}'.format(login_password, ip_port)
        # proxy_inf = 'socks5://{0}@{1}'.format(login_password, ip_port)
        proxy_data = {'http': proxy_inf,
                      'https': proxy_inf}
        return proxy_data

    def auth(self):
        try:
            self.s.headers = self.akk_obj.headers
            url = 'https://mobile.twitter.com/login'
            req = self.s.get(url)
            time.sleep(1)

            if 'JavaScript is disabled in your browser' in req.text:
                self.s.headers.update({'content-type': 'application/x-www-form-urlencoded',
                                       'referer': 'https://mobile.twitter.com/'})

                url = 'https://mobile.twitter.com/i/nojs_router?path=%2F'
                req = self.s.post(url)

            authenticity_token = re.findall(r'<input[^>]*?authenticity_token[^>]*?>', req.text)[0]
            authenticity_token = authenticity_token.split('value="')[1].split('"')[0]

            data = {'authenticity_token': authenticity_token,
                    'session[username_or_email]': self.akk_obj.login,
                    'session[password]': self.akk_obj.password,
                    'remember_me': '1',
                    'wfa': '1',
                    'commit': 'Log in ',
                    'ui_metrics': ''
                    }

            time.sleep(1)
            url = 'https://mobile.twitter.com/sessions'
            req = self.s.post(url, data=data)
            print(req.status_code)
            self.save_session()
            return req.status_code == 200
        except:
            return False

    def check_auth(self):
        url = 'https://mobile.twitter.com/'
        req = self.s.get(url)
        if '"/settings">Settings' in req.text:
            return True
        else:
            return False

    def check_and_auth(self):
        if not self.check_auth():
            return self.auth()
        else:
            return True

    def get_user_following(self):
        try:
            url = 'https://mobile.twitter.com/account'
            req = self.s.get(url)
            url = re.findall(r'href="[^"]*?following"', req.text)[0][6:-1]
            url = '{}{}'.format('https://mobile.twitter.com', url)

            time.sleep(0.5)
            req = self.s.get(url)

            # print(req.text)
            urls = re.findall(r'href=".*?profile_click', req.text)
            urls = ['https://mobile.twitter.com{}'.format(elem.split('"')[1]) for elem in urls]
            # print(urls)

            return urls
        except:
            return []

    def get_user_tweets(self, user_url, tweets_num=50):
        """ Возвращает твиты в порядке: самый первый пост самый новый"""
        try:
            url = user_url
            all_tweets = []
            for i in range(tweets_num // 30 + 1):
                if len(all_tweets) >= tweets_num:
                    break
                req = self.s.get(url)
                all_tweets.extend(self.get_page_tweets(url, req))
                url = self.get_next_page_url(req.text)
                time.sleep(1)
            return all_tweets
        except:
            return []

    def get_page_tweets(self, url, req):
        try:
            people_id = self.get_people_id(url)
            # req = self.s.get(url)
            text = re.sub(r'<a[\s\S]*?</a>', '', req.text)
            posts = re.findall(r'<div class="dir-ltr" dir="ltr">[\s\S]*?</div>', text)
            posts = [post[31:-7] for post in posts]
            posts = posts[1:]
            posts_id = re.findall(r'data-id="[^"]*?"', req.text)
            posts_id = [post_id[9:-1] for post_id in posts_id]
            posts_date = re.findall(r'<a.*?name="tweet_.*?</a>', req.text)
            posts_date = [re.sub(r'<.*?>', '', date) for date in posts_date]
            posts_zip = [Tweet(posts_id[i], posts[i], posts_date[i], people_id) for i in range(len(posts))]
            return posts_zip
        except:
            print(traceback.format_exc())
            return []

    def get_next_page_url(self, page_text):
        url = re.findall(r'href=".*?max_id.*?"', page_text)
        return 'https://mobile.twitter.com{}'.format(url[0][6:-1])

    def get_people_id(self, url: str):
        try:
            url = url.split('.com')[1]
            url = re.findall(r'/[^.?/]*', url)[0]
            return url[1:]
        except Exception as e:
            raise e

    def get_user_new_tweets(self, user_url, max_tweets_num=100, new_user_tweets=30):
        obj = AllPeoples()
        user_last_tweet_id = obj.get_last_tweet(user_url)
        if user_last_tweet_id:
            tweets = self.get_user_tweets(user_url, max_tweets_num)
        else:
            tweets = self.get_user_tweets(user_url, new_user_tweets)

        target_tweets = []
        for tweet in tweets:
            if int(tweet.id) > int(user_last_tweet_id):
                target_tweets.append(tweet)
            else:
                break

        obj.set_people_last_tweet(user_url, tweets[0].id)
        return target_tweets

    def get_all_new_tweets(self):
        urls = self.get_user_following()
        all_tweets = {}
        for url in urls:
            tweets = self.get_user_new_tweets(url)
            all_tweets[url] = tweets

        return all_tweets

    def load_session(self):
        self.s.headers = self.akk_obj.headers
        self.s.headers.update({'content-type': 'application/x-www-form-urlencoded',
                              'referer': 'https://mobile.twitter.com/'})
        self.s.cookies.update(self.akk_obj.get_cookies())

    def save_session(self):
        self.akk_obj.set_cookies(self.s.cookies.get_dict())
        self.akk_obj.save()


if __name__ == '__main__':
    obj = AkkObj('ilshatfox@gmail.com', '234qweRrTt')
    t_a = TwitterApi(obj)

    stat = t_a.check_and_auth()

    print(stat)

    # t_a.get_user_following()
    # posts = t_a.get_user_tweets('https://mobile.twitter.com/KylieJenner?p=s')
    # for i in posts:
    #     print(i.convert_to_json())
    # print(posts)

    new_tweets = t_a.get_all_new_tweets()
    pprint(new_tweets)
    # t_a.get_user_new_tweets('https://mobile.twitter.com/KylieJenner?p=s')
    #
    # check = t_a.check_auth()
    # print(check)
    # t_a.auth()
    #
    # check = t_a.check_auth()
    # print(check)





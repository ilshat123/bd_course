import json
import re
import time
import traceback


class Tweet:
    def __init__(self, id, text, post_date, people_id):
        self.id = id
        self.text = text
        self.post_date = post_date
        self.people_id = people_id

    def __str__(self):
        return 'Tweet({})'.format(str(self.__dict__))

    __repr__ = __str__

    def convert_to_json(self):
        try:
            date = re.split(r'\s+', self.post_date)
            month, day = date[0], date[1]
        except:
            month = time.strftime('%b')
            day = time.strftime('%d')
        all_dict = {'tweet_id': self.id, 'text': self.text, 'month': month, 'day': day, 'people_id': self.people_id}
        return json.dumps(all_dict)

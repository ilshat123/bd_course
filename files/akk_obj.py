import json

from files import settings
from txtWorker import TxtWorker


class AkkObj:
    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3914.0 Safari/537.36'
        }
        self.cookies = None

    def get_cookies(self) -> dict:
        if self.cookies is None:
            return {}
        else:
            return json.loads(self.cookies)

    def set_cookies(self, cookies: dict):
        self.cookies = json.dumps(cookies)

    def loads(self):
        try:
            txt_file = TxtWorker.read(settings.akk_data_txt)
        except:
            TxtWorker.write(settings.akk_data_txt, json.dumps({}))
            txt_file = TxtWorker.read(settings.akk_data_txt)
        # print(txt_file)
        dict_file = eval(txt_file)
        self.cookies = dict_file.get(self.login, '{}')

    def save(self):
        txt = {self.login: self.cookies}
        TxtWorker.write(settings.akk_data_txt, json.dumps(txt))
